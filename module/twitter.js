const Twit = require('twit');
const dotenv = require('dotenv');
const Sentiment = require('sentiment');

var sentiment = new Sentiment();
dotenv.config();

const config_twitter = {
    consumer_key: '6xY2j3V2tv81O35sCJ89aNOew',
    consumer_secret: 'YlBihNxd761A8xr7Z3M9PdfPzlmFwacK3xboTJ3yZs2nv4Ju3u',
    access_token: '138662645-p72S26UyFEQQWbbwlSyRSfh6eGQftXxzmCdVNpql',
    access_token_secret: 'CMOQmUewLfMTT4z0WQat6WQe0C0OZtLVXstx06NXMtMd2',
    timeout_ms: 60 * 1000
};

let api = new Twit(config_twitter);


class twiiter {

    get_text(tweet, key) {

        let txt = tweet.retweeted_status ? tweet.retweeted_status.full_text : tweet.full_text;
        let twt_text = txt.split(/ |\n/).filter(v => !v.startsWith('http')).join(' ');
        var kk = JSON.stringify(key);
        var frLanguage = {
            labels: { 
                'sulit': -1,
                'susah': -1,
                'lambat': -1,
                'lama': -1,
                'error': -1,
                'tidak jalan': -1,
                'ga jalan': -1,
            }
        };
        sentiment.registerLanguage('id', frLanguage);
        
        var options = {
          extras: {
            [key]: 1,
            'baik': 1,
            'keren': 1,
            'bisa': 1,
            'ajukan': 1,
          },
          language: 'id'
        };
        
        var send_ = {
            'keyword': key,
            'text': twt_text,
            'score': sentiment.analyze(twt_text, options).score,
            'comparative': sentiment.analyze(twt_text, options).comparative,
            'dataSentiment': sentiment.analyze(twt_text, options),
            'user': tweet.user.screen_name,
            'location': tweet.user.location,
            'avatar': tweet.user.profile_image_url,
            'url': 'http://twitter.com/' + tweet.user.screen_name + '/status/' + tweet.id_str,
            'from': 'Twitter',
            'allData': options
        }
        return send_;

    }

    async get_tweets(q, count, lola, radius) {
        let tweets = await api.get('search/tweets', { q, count, 'tweet_mode': 'extended', 'geocode': lola + ',' + radius });

        // return tweets.data.statuses.map(this.get_text);
         return tweets.data.statuses.map((tweet) => {
            // console.log(x);
            return this.get_text(tweet, q);
        });
    }

    async main(key, _radius, _count) {
        let keyword = key;
        let count = parseInt(_count);
        // let lola = '-6.2293867,106.6894325';
        let lola = '-6.2450355,106.8138314';
        let radius = _radius+'km'
        let tweets = await this.get_tweets(keyword, count, lola, radius);
        return tweets
    }

   
};

module.exports = twiiter;




